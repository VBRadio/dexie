#!/bin/bash
#
# 
# Dexie.sh a simple Dexcom Glucose Monitor
# for it to run correctly , it also needs
# curl, luakit and gnuplot.
# Dexie.sh was designed to run a Raspberry Pi
# and Rasbian Desktop, but could run on your
# Linux of choice. Dexie.sh downloads your
# data from Dexcom Share.
#
# Do not use Dexie.sh to make medical decisions
#
# Dexie.sh was written by:
# Frister van Bergen on August 15 2019
# https://keybase.io/vbradio
#
# Directory of script location
w=/home/pi/script/Dexcom
#
# Directory of data storage, preferable a tmpfs
p=/home/pi/tmp
#
# Low indicator on graph
low=55
#
# High indicator on graph
high=250
#
#

cp -f $w/session_id $p/session_id
cp -f $w/dex.html $p/dex.html
rm -f $p/dat.txt

t=$(date +"%m-%d_%H:%M");	
echo "$t X 0 X" >>$p/dat.txt
echo "$t X 300 X" >>$p/dat.txt

rm -f $p/response.txt

luakit $p/dex.html &


pp() {


token() {

# please change accountname and password in the below section by "-d" 

a=$(curl -sf \
  -H "Accept: application/json" -H "Content-Type: application/json" \
  -H "User-Agent: Dexcom Share/3.0.2.11 CFNetwork/711.2.23 Darwin/14.0.0" \
  -X POST https://share1.dexcom.com/ShareWebServices/Services/General/LoginPublisherAccountByName \
  -d '{"accountName":"johnsmith","applicationId":"d8665ade-9673-4e27-9ff6-92db4ce13d13","password":"12345"}' | sed 's/\"//g')

 echo $a > $p/session_id
 echo $a > $w/session_id

}


ck_token() {

x=$(cat $p/session_id)
c=$(curl -sf \
  -H "Accept: application/json" -H "Content-Type: application/json" \
  -H "User-Agent: Dexcom Share/3.0.2.11 CFNetwork/711.2.23 Darwin/14.0.0" \
  -X POST https://share1.dexcom.com/ShareWebServices/Services/Publisher/IsRemoteMonitoringSessionActive?sessionId={$x})

}


get_bg() {

x=$(cat $p/session_id)
b=$(curl -sf \
  -H "Content-Length: 0" -H "Accept: application/json" \
  -H "User-Agent: Dexcom Share/3.0.2.11 CFNetwork/672.0.2 Darwin/14.0.0" \
  -X POST "https://share1.dexcom.com/ShareWebServices/Services/Publisher/ReadPublisherLatestGlucoseValues?sessionId=$x&minutes=1440&maxCount=1")


}



ck_token
echo "checking token $x server returned -> $c"

if [ "$c" = "true" ]
then
	get_bg 
else
	echo "Getting new token.."	
	token
	get_bg
fi


# For debugging
echo $b >> $p/response.txt

echo "using    token $x"
	bg=$(echo $b | sed 's/,/ /g' | awk '{print $4}' | sed 's/\"Value\"://')
	tr=$(echo $b | sed 's/,/ /g' | awk '{print $1}' | sed 's/\"Trend\"://')
	ts=$(cat $p/ck)
	echo $tr > $p/ck

if [ "$tr" = "$ts" ]
then
	echo "No recent value"
	t=$(date +"%m-%d_%H:%M");	
	echo "$t $bg $bg $low $high" >>$p/dat.txt
	bg="No Data"

else
	echo "Latest Glucose $bg"
	t=$(date +"%m-%d_%H:%M");
	echo "$t $bg X $low $high" >>$p/dat.txt
fi


#cd $p && gnuplot -p -e 'set xdata time; set timefmt "%m-%d_%H:%M"; set format x "%H:%M\n%m-%d"; set title "Glucose Trend"; set terminal png size 800,600; set output "xyz.png"; set nokey; plot "dat.txt" using 1:2 with lines lw 2, "dat.txt" using 1:3 pt 7 lw 3 lc 7, "dat.txt" using 1:4 pt 8 lw 1, "dat.txt" using 1:5 pt 10 lw 1'

cd $p && gnuplot -p -e 'set xdata time; set timefmt "%m-%d_%H:%M"; set format x "%H:%M\n%m-%d"; set title "Glucose Trend"; set terminal png size 800,600; set output "xyz.png"; set nokey; plot "dat.txt" using 1:2 with lines lw 3, "dat.txt" using 1:3 pt 7 lw 3 lc 7, "dat.txt" using 1:4 with lines lw 1 lc 7, "dat.txt" using 1:5 with lines lw 1 lc 7'


if [ "$bg" -lt "$low" ]
then
	color="red"

else
	color="orange"
fi


if [ "$bg" -gt "$high" ]
then
	color="red"

else
	color="orange"
fi


echo "<html><head><meta http-equiv=”Pragma” content=”no-cache”><meta http-equiv=”Expires” content=”-1″><meta http-equiv=”CACHE-CONTROL” content=”NO-CACHE”></head><body bgcolor=black><meta http-equiv=refresh content=20></body>" >$p/dex.html
	echo "<center><img src=$p/xyz.png><br><font color=$color size=60><h1>$bg</h1></font></center>" >>$p/dex.html  
	echo "</body></html>" >>$p/dex.html

sleep 5m
pp

}

pp




exit;