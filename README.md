Dexie.sh a simple Dexcom Glucose Monitor.
for it to run correctly, it also needs
curl, luakit and gnuplot.

Dexie.sh was designed to run a Raspberry Pi
and Rasbian Desktop, but could run on your
Linux of choice. 

Dexie.sh downloads your data from Dexcom Share*.
A red dot / line means no recent data received.

Do **not** use Dexie.sh to make medical decisions.
Please set your accountname and password in the
token() section. 

Dexie.sh was written by:
Frister van Bergen on August 18 2019
https://gitlab.com/VBRadio/dexie/
https://keybase.io/vbradio

* Dexcom and Dexcom Share are registered trademarks of Dexcom, Inc. in the U.S., and may be registered in other countries. 